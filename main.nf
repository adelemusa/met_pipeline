/* 
 * pipeline input parameters 
 */
params.reads = "$baseDir/data/*{1,2}_001.fastq.gz"
params.paired = true
params.mapping = "$baseDir/Metadata/mapping.txt"
params.outdir = "$baseDir"
params.classifier = "sklearn"
params.MDeep = true
params.database = "silva"
params.download_classifier = true
params.import_database_qza = true
params.db_silva = "/gpfs/work/IscrC_MetaNeur_0/Database/SILVA_132_QIIME_release/rep_set/rep_set_16S_only/97/silva_132_97_16S.fna"
params.db_silva_tx = "/gpfs/work/IscrC_MetaNeur_0/Database/SILVA_132_QIIME_release/taxonomy/16S_only/97/taxonomy_7_levels.txt"
params.temp_dir = "$baseDir/temp_dir"
reference_database = "https://www.arb-silva.de/fileadmin/silva_databases/qiime/Silva_132_release.zip"
dereplication = 99

file("$baseDir/Immagini").mkdir()

println """\
         16S-METAGENOMIC - N F   P I P E L I N E    
         ===================================
         reads        : ${params.reads}
         outdir       : ${params.outdir}
         """
         .stripIndent()
         
         

 
/* 
 * define the `manifest` process that create a manifest file 
 * given the reads files
 */
 
Channel
	.fromPath(params.mapping, type: 'file')
	.ifEmpty { error "Cannot find any metadata file at this path: ${params.mapping}"  }
	.into{ ch_mapping_vis; ch_mapping_barplot; ch_mapping_diff; ch_mapping_beta; ch_mapping_alpha; ch_mapping_MDeep }
 

	
process make_temp_dir {

	script:
	
	"""
	mkdir -p ${params.temp_dir}
	export TMPDIR=${params.temp_dir}
	
	"""
}

/*
process make_SILVA_132_16S_classifier {
		publishDir "${params.outdir}/DB/", mode: 'copy'
		
		input:
		file database from ch_ref_database
		//env MATPLOTLIBRC from ch_mpl_for_make_classifier

		output:
		file("classifier.qza") into ch_qiime_classifier
		file("*.qza")
		stdout ch_message_classifier_removeHash

		script:
	  
		"""
		unzip -qq $database

		fasta=\"SILVA_132_QIIME_release/rep_set/rep_set_16S_only/${params.dereplication}/silva_132_${params.dereplication}_16S.fna\"
		taxonomy=\"SILVA_132_QIIME_release/taxonomy/16S_only/${params.dereplication}/consensus_taxonomy_7_levels.txt\"

		if [ \"${params.classifier_removeHash}\" = \"true\" ]; then
			sed \'s/#//g\' \$taxonomy >taxonomy-${params.dereplication}_removeHash.txt
			taxonomy=\"taxonomy-${params.dereplication}_removeHash.txt\"
			echo \"\n######## WARNING! The taxonomy file was altered by removing all hash signs!\"
		fi

		### Import
		qiime tools import --type \'FeatureData[Sequence]\' \
			--input-path \$fasta \
			--output-path ref-seq-${params.dereplication}.qza
		qiime tools import --type \'FeatureData[Taxonomy]\' \
			--input-format HeaderlessTSVTaxonomyFormat \
			--input-path \$taxonomy \
			--output-path ref-taxonomy-${params.dereplication}.qza

		#Extract sequences based on primers
		qiime feature-classifier extract-reads \
			--i-sequences ref-seq-${params.dereplication}.qza \
			--p-f-primer ${params.FW_primer} \
			--p-r-primer ${params.RV_primer} \
			--o-reads ${params.FW_primer}-${params.RV_primer}-${params.dereplication}-ref-seq.qza \
			--quiet

		#Train classifier
		qiime feature-classifier fit-classifier-naive-bayes \
			--i-reference-reads ${params.FW_primer}-${params.RV_primer}-${params.dereplication}-ref-seq.qza \
			--i-reference-taxonomy ref-taxonomy-${params.dereplication}.qza \
			--o-classifier ${params.FW_primer}-${params.RV_primer}-${params.dereplication}-classifier.qza \
			--quiet
		"""
	}
	ch_message_classifier_removeHash
		.subscribe { log.info it }

*/
	


file("${params.outdir}/QZV_dir").mkdirs()



if (params.paired) {

Channel 
    .fromFilePairs( params.reads )
    .ifEmpty { error "Cannot find any reads matching: ${params.reads}"  }
    .into{ read_pairs_ch } 

read_pairs_ch
	.map { name, tuple -> [name.split("_L001")[0], tuple[0], tuple[1]] } 
	.map { name, forward, reverse -> [ name +"\t"+ forward + "\t" + reverse  ] } 
	.flatten()
	.collectFile(name: 'manifest.txt', newLine: true, storeDir: "${params.outdir}/Metadata", seed: "sample-id\tforward-absolute-filepath\treverse-absolute-filepath")
	.set { ch_manifest } 
	
process qiime_import_paired {
	
	publishDir "${params.outdir}", mode: 'copy'
	
	input:
	file(manifest) from ch_manifest

	output:
	file "data.qza" into (ch_qiime_import, ch_qiime_import_denoise)
	
	conda "/home/adelaide/anaconda3/envs/qiime2-2020.2"
	"""
	qiime tools import \
	--type 'SampleData[PairedEndSequencesWithQuality]' \
	--input-path ${manifest} \
	--output-path data.qza \
	--input-format PairedEndFastqManifestPhred33V2
	
	"""
}
	
process dada_denoise_paired {
	
	input:
	file(data) from ch_qiime_import_denoise
	
	output:
	file "dada2_data_table.qza" into(ch_table_qza_vis, ch_table_qza_barplot, ch_table_qza_diff, ch_table_qza_alpha, ch_table_qza_beta, ch_table_qza_MDeep )
	file "dada2_data_rep_seqs.qza" into(ch_rep_seq_vis, ch_rep_seq_phy, ch_rep_seq_classifier)
	file "dada2_data_stats.qza" into ch_data_stats_vis
	
	conda "/home/adelaide/anaconda3/envs/qiime2-2020.2"

	"""
	qiime dada2 denoise-paired \
  	--i-demultiplexed-seqs ${data} \
  	--p-trim-left-f 0 \
  	--p-trim-left-r 0 \
  	--p-trunc-len-f 0 \
  	--p-trunc-len-r 0 \
  	--p-max-ee-f 4 \
  	--p-max-ee-r 4 \
  	--o-table dada2_data_table.qza \
  	--o-representative-sequences dada2_data_rep_seqs \
  	--o-denoising-stats dada2_data_stats  \
  	--p-n-threads 18
  	"""

}
}else {

Channel 
    .fromPath( params.reads )
    .println()
    
Channel 
    .fromPath( params.reads )
    .ifEmpty { error "Cannot find any reads matching: ${params.reads}"  }
    .into{read_single_ch} 
    

read_single_ch
	.map { name -> [name.baseName.split("_L001")[0] , name] } 
	.map { name, path -> [ name +"\t"+ path ] } 
	.flatten()
	.collectFile(name: 'manifest.txt', newLine: true, storeDir: "${params.outdir}/Metadata", seed: "sample-id\tabsolute-filepath")
	.set { ch_manifest } 
	
process qiime_import_single {
	
	publishDir "${params.outdir}", mode: 'copy'
	
	input:
	file(manifest) from ch_manifest

	output:
	file "data.qza" into (ch_qiime_import, ch_qiime_import_denoise)
	
	conda "/home/adelaide/anaconda3/envs/qiime2-2020.2"
	
	"""
	
	qiime tools import \
	--type 'SampleData[SequencesWithQuality]' \
	--input-path ${manifest} \
	--output-path data.qza \
	--input-format SingleEndFastqManifestPhred33V2
	
	"""
}

process dada_denoise_single {
	
	input:
	file(data) from ch_qiime_import_denoise
	
	output:
	file "dada2_data_table.qza" into(ch_table_qza_vis, ch_table_qza_barplot, ch_table_qza_diff, ch_table_qza_alpha, ch_table_qza_beta, ch_table_qza_MDeep )
	file "dada2_data_rep_seqs.qza" into(ch_rep_seq_vis, ch_rep_seq_phy, ch_rep_seq_classifier)
	file "dada2_data_stats.qza" into ch_data_stats_vis
	
	conda "/home/adelaide/anaconda3/envs/qiime2-2020.2"

	"""
	qiime dada2 denoise-single \
  	--i-demultiplexed-seqs ${data} \
  	--p-trim-left 0 \
  	--p-trunc-len 0 \
  	--p-max-ee 4 \
  	--o-table dada2_data_table.qza \
  	--o-representative-sequences dada2_data_rep_seqs \
  	--o-denoising-stats dada2_data_stats  \
  	--p-n-threads 18
  	"""

}


}


process qiime_demux {

	publishDir "${params.outdir}/QZV_dir", mode: 'copy'

	input:
	file(data) from ch_qiime_import
	
	conda "/home/adelaide/anaconda3/envs/qiime2-2020.2"

	"""
	qiime demux summarize \
  	--i-data ${data} \
  	--o-visualization summary_raw_data \
  	--verbose
  	"""

}

process stats_visualization {

	publishDir "${params.outdir}/QZV_dir", mode: 'copy'
	
	input:
	file(dada2_data_table) from ch_table_qza_vis
	file(dada2_data_rep_seqs) from ch_rep_seq_vis
	file(dada2_data_stats) from ch_data_stats_vis
	file(mapping) from ch_mapping_vis
	

	conda "/home/adelaide/anaconda3/envs/qiime2-2020.2"

	"""

	qiime metadata tabulate \
  	--m-input-file ${dada2_data_stats} \
  	--o-visualization dada2_data_stats.qzv

	qiime feature-table summarize \
  	--i-table ${dada2_data_table} \
  	--o-visualization dada2_data_table.qzv \
  	--m-sample-metadata-file ${mapping}

	qiime feature-table tabulate-seqs \
  	--i-data ${dada2_data_rep_seqs} \
  	--o-visualization dada2_data_rep_seqs.qzv
  
	"""
}

process qiime_phylogeny {

	input:
	
	file(dada2_data_rep_seqs) from ch_rep_seq_phy
	
	output:
	
	file("aligned_seqs_data.qza") into ch_al_seq_data
	file("aligned_seqs_masked_data.qza") into ch_al_seq_mask_data
	file("unrooted_tree.qza") into ch_unrooted
	file("rooted_tree.qza") into(ch_rooted_diff, ch_rooted_alpha, ch_rooted_beta, ch_rooted_MDeep )
	
	conda "/home/adelaide/anaconda3/envs/qiime2-2020.2"

	"""

	qiime phylogeny align-to-tree-mafft-fasttree \
  	--i-sequences ${dada2_data_rep_seqs} \
  	--o-alignment aligned_seqs_data.qza \
  	--o-masked-alignment aligned_seqs_masked_data.qza \
  	--o-tree unrooted_tree.qza \
  	--o-rooted-tree rooted_tree.qza

	"""

}

// Download the classifiers if not already present

if( params.download_classifier ){
	process download_classifiers {
	
		publishDir "${params.outdir}/Taxonomic_assignment", mode: 'copy'
	
		output:
	
		file("silva_classifier.qza") into ch_silva_classifier
		file("gg-13-8-99-nb-classifier.qza") into ch_gg_classifier
	

		"""
	
		wget https://data.qiime2.org/2020.2/common/silva-132-99-nb-classifier.qza
		wget https://data.qiime2.org/2020.2/common/gg-13-8-99-nb-classifier.qza
		mv silva-132-99-nb-classifier.qza silva_classifier.qza
		mv gg-13-8-99-nb-classifier.qza gg_classifier.qza
	
		"""
	}

}else {
	if ( params.database == "silva") {
		Channel
		.fromPath("${baseDir}/Taxonomic_assignment/silva_classifier.qza", type: 'file')
		.ifEmpty { error "Cannot find any classifier file at this path:${baseDir}/Taxonomic_assignment/silva_classifier.qza "  }
		.set{ ch_silva_classifier }
	}else{
		Channel
		.fromPath("${baseDir}/Taxonomic_assignment/gg_classifier.qza", type: 'file')
		.ifEmpty { error "Cannot find any classifier file at this path: ${baseDir}/Taxonomic_assignment/gg_classifier.qza" }
		.set{ ch_gg_classifier }
	}
	

}

/* Importa database SILVA (sequenze e tassonomia) in .qza */
if (params.import_database_qza){
if (params.database == "silva") {
	process import_taxonomy_qza_silva{

		output:
	
		file("SILVA_seq.qza") into ch_silva_seq_qza
		file("SILVA_tax.qza") into ch_silva_tax_qza
		
		conda "/home/adelaide/anaconda3/envs/qiime2-2020.2"

		"""
	
		qiime tools import \
  		--input-path ${params.db_silva} \
  		--output-path SILVA_seq.qza  \
  		--type 'FeatureData[Sequence]'

		qiime tools import \
 		--input-path ${params.db_silva_tx} \
 		--output-path SILVA_tax.qza  \
 		--type 'FeatureData[Taxonomy]' \
 		--input-format HeaderlessTSVTaxonomyFormat  
 		
 		"""
 	}
 	
 }else if (params.database == "gg") {
 	
 	process import_taxonomy_qza_gg{

	output:
	
	file("GG_seq.qza") into ch_gg_seq_qza
	file("GG_tax.qza") into ch_gg_tax_qza

	"""
	
	qiime tools import \
  	--input-path ${params.db_gg} \
  	--output-path GG_seq.qza  \
  	--type 'FeatureData[Sequence]'

	qiime tools import \
 	--input-path ${params.db_gg_tx} \
 	--output-path GG_tax.qza  \
 	--type 'FeatureData[Taxonomy]' \
 	--input-format HeaderlessTSVTaxonomyFormat  
 		
 	"""
 	}
 }
}else {

ch_silva_seq_qza = Channel.fromPath("${baseDir}/Taxonomic_assignment/SILVA_seq.qza")
ch_silva_tax_qza = Channel.fromPath("${baseDir}/Taxonomic_assignment/SILVA_tax.qza")

}
 

//taxonomy assignment

if (params.classifier == "sklearn"){

	if (params.database == "silva") {

		process classify_sklearn_silva {
	
		input:
		
		file(silva_classifier) from ch_silva_classifier
		file(dada2_data_rep_seqs) from ch_rep_seq_classifier
	
		output:
	
		file("taxonomy_classification_data.qza") into(ch_classification_barplot, ch_classification_alpha, ch_classification_beta, ch_classification_ab_diff, ch_classification_MDeep)
	
		conda "/home/adelaide/anaconda3/envs/qiime2-2020.2"
	
		"""
	
		qiime feature-classifier classify-sklearn \
  		--i-classifier ${silva_classifier} \
  		--i-reads ${dada2_data_rep_seqs} \
  		--o-classification taxonomy_classification_data.qza
  	
  		"""
  	
  		}
 	}else if ( params.database == "gg") {
 
		process classify_sklearn_gg {
	
		input:
	
		file(gg_classifier) from ch_gg_classifier
		file(dada2_data_rep_seqs) from ch_rep_seq_classifier
	
		output:
	
		file("taxonomy_classification_data.qza") into(ch_classification_alpha, ch_classification_beta, ch_classification_ab_diff, ch_classification_MDeep)
	
		conda "/home/adelaide/anaconda3/envs/qiime2-2020.2"
	
		"""
	
		qiime feature-classifier classify-sklearn \
  		--i-classifier ${gg_classifier} \
  		--i-reads ${dada2_data_rep_seqs} \
  		--o-classification taxonomy_classification_data.qza
  	
  		"""
  	
  		}
  	}
  		
  	process barplot_sklearn {

	publishDir "${params.outdir}/QZV_DIR", mode: 'copy'

	input:
	
	file(dada2_data_table) from ch_table_qza_barplot
	file(taxonomy_classification_data) from ch_classification_barplot
	file(mapping) from ch_mapping_barplot
	
	output:
	
	file("barplot_data.qzv") into ch_vis

	"""
	
	qiime taxa barplot \
	--i-table ${dada2_data_table} \
 	--i-taxonomy ${taxonomy_classification_data} \
 	--m-metadata-file ${mapping} \
 	--o-visualization barplot_data.qza
 	
 	"""
	}
 
 }else if (params.classfier == "vsearch") {
 	
 	if (params.database == "silva") {

		process classify_vsearch_silva {
	
		input:
		
		file(SILVA_seq) from ch_silva_seq_qza
		file(SILVA_tax) from ch_silva_tax_qza
		file(dada2_data_rep_seqs) from ch_rep_seq_classifier
	
		output:
	
		file("taxonomy_classification_data.qza") into(ch_classification_alpha, ch_classification_beta, ch_classification_ab_diff, ch_classification_MDeep)
	
		conda "/home/adelaide/anaconda3/envs/qiime2-2020.2"
	
		"""
	
		qiime feature-classifier classify-consensus-vsearch \
		--i-query ${dada2_data_rep_seqs} \
 		--i-reference-reads ${SILVA_seq}  \
 		--i-reference-taxonomy ${SILVA_tax} \
 		--p-threads 18 \
 		--o-classification taxonomy_classification_data.qza
  	
  		"""
  	
  		}
 	}else if ( params.database == "gg") {
 
		process classify_vsearch_gg {
	
		input:
	
		file(GG_seq) from ch_gg_seq_qza
		file(GG_tax) from ch_gg_tax_qza
		file(dada2_data_rep_seqs) from ch_rep_seq_classifier
	
		output:
	
		file("taxonomy_classification_data.qza") into(ch_classification_alpha, ch_classification_beta, ch_classification_ab_diff, ch_classification_MDeep)
	
		conda "/home/adelaide/anaconda3/envs/qiime2-2020.2"
	
		"""
	
		qiime feature-classifier classify-consensus-vsearch \
		--i-query ${dada2_data_rep_seqs} \
 		--i-reference-reads ${GG_seq}  \
 		--i-reference-taxonomy ${GG_tax} \
 		--p-threads 18 \
 		--o-classification taxonomy_classification_data.qza
  	
  		"""
  	
  		}
  	}
  		
  	process barplot_vsearch {

	publishDir "${params.outdir}/QZV_DIR", mode: 'copy'

	input:
	
	file(dada2_data_table) from ch_table_qza_barplot
	file(taxonomy_classification_data) from ch_classification_barplot
	file(mapping) from ch_mapping_barplot
	
	output:
	
	file("barplot_data.qzv") into ch_vis

	"""
	
	qiime taxa barplot \
	--i-table ${dada2_data_table} \
 	--i-taxonomy ${taxonomy_classification_data} \
 	--m-metadata-file ${mapping} \
 	--o-visualization barplot_data.qzv
 	
 	"""
 	}
 	
}else if (params.classfier == "blast") { 
 	
 	if (params.database == "silva") {

		process classify_blast_silva {
	
		input:
		
		
		file(SILVA_seq) from ch_silva_seq_qza
		file(SILVA_tax) from ch_silva_tax_qza
		file(dada2_data_rep_seqs) from ch_rep_seq_classifier
	
		output:
	
		file("taxonomy_classification_data.qza") into(ch_classification_alpha, ch_classification_beta, ch_classification_ab_diff, ch_classification_MDeep)
	
		conda "/home/adelaide/anaconda3/envs/qiime2-2020.2"
	
		"""
	
		qiime feature-classifier classify-consensus-blast \
		--i-query ${dada2_data_rep_seqs} \
 		--i-reference-reads ${SILVA_seq} \
 		--i-reference-taxonomy ${SILVA_tax} \
 		--o-classification taxonomy_classification_data.qza
  	
  		"""
  	
  		}
 	}else if ( params.database == "gg") {
 
		process classify_blast_gg {
	
		input:
	
		file(GG_seq) from ch_gg_seq_qza
		file(GG_tax) from ch_gg_tax_qza
		file(dada2_data_rep_seqs) from ch_rep_seq_classifier
	
		output:
	
		file("taxonomy_classification_data.qza") into(ch_classification_alpha, ch_classification_beta, ch_classification_ab_diff, ch_classification_MDeep)
	
		conda "/home/adelaide/anaconda3/envs/qiime2-2020.2"
	
		"""
	
		qiime feature-classifier classify-consensus-blast \
		--i-query ${dada2_data_rep_seqs} \
 		--i-reference-reads ${GG_seq} \
 		--i-reference-taxonomy ${GG_tax} \
 		--o-classification taxonomy_classification_data.qza
 		
  	
  		"""
  	
  		}
  	}
  		
  	process barplot_blast {

	publishDir "${params.outdir}/QZV_DIR", mode: 'copy'

	input:
	
	file (dada2_data_table) from ch_table_qza_barplot
	file (taxonomy_classification_data) from ch_classification_barplot
	file (mapping) from ch_mapping_barplot
	
	output:
	
	file("barplot_data.qzv") into ch_vis

	"""
	
	qiime taxa barplot \
	--i-table ${dada2_data_table} \
 	--i-taxonomy ${taxonomy_classification_data} \
 	--m-metadata-file ${mapping} \
 	--o-visualization barplot_data.qzv
 	
 	"""
 	
	}
	
		
 }
 
 
	
//Analysis  

ch_rooted_beta
	.combine(ch_table_qza_beta)
	.combine(ch_classification_beta)
	.combine(ch_mapping_beta)
	.set{ch_beta_diversity}
	
ch_rooted_alpha
	.combine(ch_table_qza_alpha)
	.combine(ch_classification_alpha)
	.combine(ch_mapping_alpha)
	.set{ch_alpha_diversity}
	
ch_rooted_diff
	.combine(ch_table_qza_diff)
	.combine(ch_classification_ab_diff)
	.combine(ch_mapping_diff)
	.set{ch_diff_abundancy}

ch_rooted_MDeep
	.combine(ch_table_qza_MDeep)
	.combine(ch_classification_MDeep)
	.combine(ch_mapping_MDeep)
	.set{ch_MDeep}
		

process alpha_diversity {

	publishDir "${params.outdir/immagini}", mode: 'copy' 

	input:
	
	set file(rooted_tree), file(dada2_data_table), file(taxonomy_classification_data), file(mapping) from ch_alpha_diversity
	
	output:
	
	file("alpha-diversity/*")
	
	when:
	
	category.length() > 0
	
	"""
	IFS=',' read -r -a metacategory <<< \"$category\"
	
	for j in \"\${metacategory[@]}\"
		do
			alphaDiversity.R ${dada2_data_table} ${mapping} ${rooted_tree} ${taxonomy_classification_data} ${j}
	done
		
	"""

}

process beta_diversity { 

	input:
	
	set file(rooted_tree), file(dada2_data_table), file(taxonomy_classification_data), file(mapping) from ch_beta_diversity
	
	conda "/home/adelaide/anaconda3/envs/metaRJupy"
	
	"""
	beta_diversity.R ${dada2_data_table} ${mapping} ${rooted_tree} ${taxonomy_classification_data} "IBD"
	
	"""

}



process differential_abundancy {

	publishDir "${params.outdir/immagini}", mode: 'copy' 

	input:
	
	set file(rooted_tree), file(dada2_data_table), file(taxonomy_classification_data), file(mapping) from ch_diff_abundancy
	
	output:
	
	file("differential-abundance/*")
	
	"""
	IFS=',' read -r -a metacategory <<< \"$category\"
	IFS=',' read -r -a levels <<< \"$levels\"
	
	for j in \"\${metacategory[@]}\"
		for i in \"\${levels[@]}\"
			do
				abbDiff_WC.R ${dada2_data_table} ${mapping} ${rooted_tree} ${taxonomy_classification_data} ${j} ${i}
	
		done
	done
		
	"""



}

//Machine Learning

if (params.MDeep == true) {

process MDeep_C_matrix {

	publishDir "${params.outdir}/MDeep_master/data", mode: 'copy'

	input:
	
	set file(rooted_tree), file(dada2_data_table), file(taxonomy_classification_data), file(mapping) from ch_MDeep
	
	output:
	
	file("c.csv") into ch_c_MDeep
	file("otu_table.txt") into ch_otu_MDeep
	file("metadata.txt") into ch_meta_MDeep
	
	"""
	
	input_MDeep.R ${dada2_data_table} ${mapping} ${rooted_tree} ${taxonomy_classification_data}
	
	"""

}


ch_otu_MDeep
	.combine(ch_meta_MDeep)
	.set{ch_dataset}	
	

process MDeep_split_dataset {

	publishDir "${params.outdir}/MDeep_master/data", mode: 'copy'

	input:
	
	set file(otu), file(meta) from ch_dataset
	
	output:
	
	file("X_train.npy") into ch_x_train
	file("Y_train.npy") into ch_y_train
	file("X_eval.npy") into ch_x_eval
	file("Y_eval.npy") into ch_y_eval
	
	"""
	
	IFS=',' read -r -a variable <<< \"$variable\"
	
	dataset.py \"\${variable}\" ${otu} ${meta}
	
	"""

}


ch_c_MDeep
	.combine(ch_x_train)
	.combine(ch_y_train)
	.combine(ch_x_eval)
	.combine(ch_y_eval)
	.set{ch_MDeep_input}	

process MDeep {

	input:
	
	set file(c_matrix), file(x_train), file(y_train), file(x_eval), file(y_eval), file(x_test) from ch_MDeep_input
	
	output:	
	
	
	"""
	IFS=',' read -r -a out_type <<< \"$out_type\"
		
	bin/MDeep_master/src/MDeep.py --correlation_file ${c_matrix} --xtrain_file ${x_train} --ytrain_file ${y_train} --xeval_file ${x_eval} --yeval_file ${y_eval} --outcome_type \"\${out_type}\"
	
	"""

}
}




workflow.onComplete { 
	println ( workflow.success ? "\nDone!" : "Oops .. something went wrong" )
}

