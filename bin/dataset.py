#!/usr/bin/env python3

import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
import sys 

#argument check
if len(sys.argv) != 3:
    exit("Missing input args")

variable = sys.argv[0]
deseq2_otu = sys.argv[1] #data
deseq2_met = sys.argv[2] #metadata

Deseq2_otu = read_csv(deseq2_otu)
Deseq2_met = read_csv(deseq2_met)


Deseq2_otu = pd.DataFrame.transpose(Deseq2_otu) #transpose the dataframe 
Deseq2_otu = Deseq2_otu.sort_index() 


Deseq2_otu[variable] = Deseq2_met[variable]

#remove repeted columns
Deseq2_otu = Deseq2_otu.loc[:,~Deseq2_otu.columns.duplicated()]

y = Deseq2_otu[variable]
X = Deseq2_otu.drop(columns=variable)

scaler_z = StandardScaler()

X = scaler.fit_transform(X)

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.33, random_state=42)

np.save("x_train.npy", X_train)
np.save("y_train.npy", y_train)
np.save("x_test.npy", X_test)
np.save("y_test.npy", y_test)
