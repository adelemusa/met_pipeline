beta_diversity <- function(phy,level,variable,v){
    phyLevel <- tax_glom(phy,level)
    phyLevel <- check_dataset(phyLevel)
    phy_sub <- eval(parse(text=paste("subset_samples( phyLevel,",paste(to_vec(for(x in variable) for(y in c(v)) paste(x, y, sep="==")), collapse = " | "),")")))
    phyObj <- Deseq2_norm(phy_sub, variable)
    vars <- unique(sample_data(phyObj)[,variable])
    grafici <- list(); i = 1;
    for(distance in c("bray", "unifrac", "wunifrac", "canberra", "euclidean")){
        G <- testPermanovaPCoA(phyObj,DistancePhyl=distance,Variable=variable,Permutations=9999)
        grafici[[i]] <- G; i <- i + 1;
    }
    P <- ggarrange(grafici[1],grafici[2],grafici[3],grafici[4],ncol=2,nrow=2,labels=c("A","B","C","D"))
    P <- ggarrange(grafici[[1]],grafici[[2]],grafici[[3]],grafici[[4]],ncol=2,nrow=2,common.legend=TRUE)
    Save.graph(Graph=P, Name=paste("Beta_diversity", paste(to_vec(for(x in variable) for(y in vars) paste(x, y, sep=":")), collapse="_"), sep="_"), Name_prefix=level)
}
                                                                                        
alpha_diversity<- function(phy,level,variable,v){
    phyLevel <- tax_glom( phy, level , NArm=FALSE)
    phyLevel <- check_dataset(phyLevel)
    phySub <- eval(parse(text=paste("subset_samples( phyLevel,",paste(to_vec(for(x in variable) for(y in c(v)) paste(x, y, sep="==")), collapse = " | "),")")))
    phyObj <- Deseq2_norm(phySub, variable )
    vars <- unique(sample_data(phyObj)[,variable])
    cols = gg_color_hue(lengths(vars))
    DIV <- estimate_richness(phyObj)
    rownames(DIV) <- gsub("\\.","-",rownames(DIV))
    measures_metadata_rare <- merge( sample_data(phyObj) , DIV , by="row.names" , all = TRUE )
    rownames(measures_metadata_rare) <- measures_metadata_rare$Row.names
    measures_metadata_rare$Row.names <- NULL 
    measures_metadata_reduced <- measures_metadata_rare[,c(variable,"Observed","Chao1","Simpson","Shannon")]
    grafici <- list(); i = 1;
    for (c in 2:ncol(measures_metadata_reduced)){
        variabile <- colnames(measures_metadata_reduced)[c]
        X <- as.factor(measures_metadata_reduced[,1])
        Y <- measures_metadata_reduced[,c]
        KW <- kruskal.test( Y ~ X )
        PVal <- KW$p.value
        PVal <- formatC(PVal, format = "e", digits = 3)
        G <- makeBarplot_colors( dataframe=measures_metadata_rare, xvalues=X, yvalues=Y, xlabel=variable, ylabel=variabile, cols=cols, title="")
        G <- G + annotate(geom="text", label=paste("p = ", PVal) , color="red", x = Inf, y = Inf, hjust = 1, vjust = 1, fontface="bold")
        grafici[[i]] <- G; i <- i + 1;
    }

    P <- ggarrange(grafici[[1]],grafici[[2]],grafici[[3]],grafici[[4]],ncol=2,nrow=2,common.legend=TRUE)
    Save.graph( Graph=P , Name=paste("Alfa_Diversity", paste(to_vec(for(x in variable) for(y in vars) paste(x, y, sep=":")), collapse="_"), sep="_"), Name_prefix=level )
    
}

dada2_univariate_analysis <- function(phy,level,variable,v){
    phy_sub <- eval(parse(text=paste("subset_samples( phy,",paste(to_vec(for(x in variable) for(y in c(v)) paste(x, y, sep="==")), collapse = " | "),")")))
    phyLevel <- tax_glom(phy_sub,level)
    phyLevel <- check_dataset(phyLevel)
    vars <- unique(sample_data(phyLevel)[,variable])
    Q <- phyloseq_to_deseq2(phyLevel, ~ IBD)
    geoMeans = apply(counts(Q),1,gm_mean)
    D <- estimateSizeFactors(Q, geoMeans = geoMeans)
    dds <- DESeq(D)
    normalized_count <- as.data.frame(round(counts(D,normalized=TRUE)))
    otu_table(phyLevel) <- otu_table(normalized_count, taxa_are_rows=TRUE)
    colnames(normalized_count) <- gsub("\\.","-",colnames(normalized_count))
    otu_table(phyLevel) <- otu_table(normalized_count, taxa_are_rows=TRUE)
    res <- as.data.frame(results(dds))
    res <- res[order(res$padj),]
    sigtab = res[which(res$pvalue < alpha), ]
    sigtab = cbind(as(sigtab, "data.frame"), as(tax_table(phyLevel)[rownames(sigtab), ], "matrix"))
    sigtab <- sigtab[order(sigtab$pvalue),]
    qiimedata <- phyLevel
    qiimedata_perce <- transform_sample_counts(qiimedata, function(x) {x/sum(x)} )
    qiimedata_sub = prune_taxa(rownames(sigtab),qiimedata_perce)
    dat <- psmelt(qiimedata_sub)
    dat[,level] <- as.character(dat[,level])
    dat$Abundance <- dat$Abundance+0.000001
    cols = gg_color_hue(length(unique(dat[,variable])))
    D<-ggplot(data = dat, aes(x=dat[,level], y=Abundance, fill=dat[,variable])) + geom_boxplot(notch=FALSE,outlier.size=0) +
    geom_point(pch = 21, position = position_jitterdodge())+
    theme(axis.text.x=element_text(angle=45,hjust=1,vjust=1)) +
    scale_y_continuous(limits = c(1e-6, 1e0), breaks = c(1e-6, 1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1e0),trans="log10",labels = trans_format("log10", math_format(10^.x))) +
    labs(x = level, y="Proportion") + scale_fill_manual(values=c(cols[1],cols[2])) +
    guides(fill=guide_legend(title=paste(variable)))
    Save.graph( Graph=D , Name=paste("Deseq2_differential_abundance",paste(to_vec(for(x in variable) for(y in vars) paste(x, y, sep=":")), collapse="_"),sep="_") , Name_prefix=level)
}

multivariate_analysis <- function(phy,level,variable,vars,v){
    phyLevel <-tax_glom(phy,level)
    phyLevel <- check_dataset(phyLevel)
    phy_sub <- eval(parse(text=paste("subset_samples( phyLevel,",paste(to_vec(for(x in variable) for(y in c(v)) paste(x, y, sep="==")), collapse = " | "),")")))
    phyObj <- Deseq2_norm(phy_sub, variable)
    metadata <- as(sample_data(phyObj),"data.frame")[,vars]
    metadata <- metadata[rowSums(is.na(metadata)) != ncol(metadata),]
    metadata <- metadata[apply(metadata[,-1], 1, function(x) !all(x==0)),]
    metadata <- as(metadata,"data.frame")
    Formula <- paste(vars, collapse="+")
    h <- hash()
    parameters <- c()       
    for(dist in c("bray","unifrac","wunifrac","canberra")){
        dm <- phyloseq::distance(phy_sub,method=dist)
        parameters <- c(parameters,PERMANOVA_stepwise_backward(metadata,dm,parameters=Formula))
        h[[dist]] <- parameters    
    } 
    return(h)
}

unbalanced_variables <- function(phy,variable,num,categ,v){
    phy_sub <- eval(parse(text=paste("subset_samples( phy,",paste(to_vec(for(x in variable) for(y in c(v)) paste(x, y, sep="==")), collapse = " | "),")")))
    phy_sub <- check_dataset(phy_sub) 
    metadata <- sample_data(phy_sub)
    variables  <- c(num,categ)                                                                     
    vars <- unique(metadata[[variable]])                                                                
    metadata <- metadata[,variables]
    metadata[,categ] <- sapply(metadata[,categ], as.factor)
    metadata[,num] <- sapply(metadata[,num], as.numeric)
    m <- as.data.frame(metadata[,variable]); rownames(m) <- rownames(metadata); colnames(m) <- c(variable)
    metadata <- metadata[ , !(colnames(metadata) %in% c(variable))]
    row_names <- c()
    pvalues <- c()
    cat_0_stats <- c()
    cat_1_stats <- c()
    for(col in 1:ncol(metadata)){
        print(colnames(metadata)[col])
        var <- colnames(metadata)[col]
        val <- metadata[[var]]
        cat("Analisi della variabile: ", var ,"\n")
        y=ifelse(m[,variable]=="1",1,0)
        pval <- round((summary(glm(y~val,family=binomial))$coef[2,4]),digits=2)
        cat("\nP-value dell'analisi: ", pval ,"\n")
        val <- metadata[[var]]
        df <- mergia.dataframe(m,as.vector(metadata[,col]))
        df_0 <- subset(df, df[,variable]==vars[1])
        df_1 <- subset(df, df[,variable]==vars[2])
        if(var %in% categ){
            cat("\n\t Variabile categoriale")
            prop_table_0 <- prop.table(table(df_0))
            prop_table_1 <- prop.table(table(df_1))
            cols_names_prop_0 <- colnames(prop_table_0)
            rows_val_prop_0 <-  round(as.vector(prop_table_0[1,]),digits=2)
            row_0 <- c(rbind(cols_names_prop_0,rows_val_prop_0))
            new_row_0 <- c()
            for ( i in seq(1,length(row_0),2) ){
                element <- paste( c(row_0[i], row_0[i+1]) ,collapse="=")
                new_row_0 <- c( new_row_0 , element )
            }
            str_0 <- paste(new_row_0,collapse=" , ")
            cols_names_prop_0 <- colnames(prop_table_1)
            print(prop_table_1)
            rows_val_prop_1 <- round(as.vector(prop_table_1[1,]),digits=2)
            row_1 <- c(rbind(cols_names_prop_0,rows_val_prop_1))
            new_row_1 <- c()
            for ( i in seq(1,length(row_1),2) ){
                element <- paste( c(row_1[i], row_1[i+1]) ,collapse="=")
                new_row_1 <- c( new_row_1 , element )
            }
            str_1 <- paste(new_row_1,collapse=" , ")
        } else {
            cat("\n\t Variabile numerica")
            print(df_0[,2])
            mean_0 <- mean(df_0[,2]); mean_1 <- mean(df_1[,2]);
            median_0 <- median(df_0[,2]); median_1 <- median(df_1[,2]);
            devstd_0 <- sd(df_0[,2]); devstd_1 <- sd(df_0[,2])
            min_0 <- min(df_0[,2]) ; min_1 <- min(df_1[,2]);
            max_0 <- max(df_0[,2]) ; max_1 <- max(df_1[,2]);
            mean_0 <- round(mean_0,digits=2); mean_1 <- round(mean_1,digits=2);
            median_0 <- round(median_0,digits=2); median_1 <- round(median_1,digits=2);
            devstd_0 <- round(devstd_0,digits=2); devstd_1 <- round(devstd_1,digits=2);
            min_0 <- round(min_0,digits=2); min_1 <- round(min_1,digits=2);
            max_0 <- round(max_0,digits=2); max_1 <- round(max_1,digits=2);
            str_0 <- paste(" Mean: ",mean_0," Median: ",median_0," Dev.Std: ",devstd_0," Min: ",min_0," Max: ",max_0)
            str_1 <- paste(" Mean: ",mean_1," Median: ",median_1," Dev.Std: ",devstd_1," Min: ",min_1," Max: ",max_1)
            cat("\n\t ",str_0,"\n")
            cat("\n\t ",str_1,"\n")
        }
        row_names <- c(row_names, var)
        pvalues <- c(pvalues, pval)
        cat_0_stats <- c(cat_0_stats, str_0)
        cat_1_stats <- c(cat_1_stats, str_1)
        cat("\n")
    }

    f_df <- data.frame(pvalues,cat_0_stats,cat_1_stats)
    rownames(f_df) <- row_names
    colnames(f_df) <- c("p-value",vars[1],vars[2])
    write.table(f_df,paste("unbalanced_variables",paste(to_vec(for(x in variable) for(y in vars) paste(x, y, sep=":")), collapse="_"),sep="_"),sep="\t",quote=FALSE)
}
