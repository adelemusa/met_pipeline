#!/usr/bin/R

source("/home/adelaide/adelaide/Dottorato/Nextflow/met_pipeline/bin/metagenomicFunction.R")
source("/home/adelaide/adelaide/Dottorato/Nextflow/met_pipeline/bin/beta_diversity.R")

library('qiime2R')
library('phyloseq')
library('ggplot2')
library('vegan')
