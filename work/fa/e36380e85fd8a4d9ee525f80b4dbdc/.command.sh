#!/bin/bash -ue
Rscript --vanilla /home/adelaide/adelaide/Dottorato/Nextflow/met_pipeline/bin/beta_diversity.R dada2_gastrodata_table.qza mapping.txt rooted_tree.qza taxonomy_classification_data_sklearn.qza "IBD"
