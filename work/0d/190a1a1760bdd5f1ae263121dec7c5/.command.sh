#!/usr/bin/R
	
	source("/home/adelaide/adelaide/Dottorato/Nextflow/met_pipeline/bin/metagenomicFunction.R")
	source("/home/adelaide/adelaide/Dottorato/Nextflow/met_pipeline/bin/beta_diversity.R")
	
	library('qiime2R')
	library('phyloseq')
	library('ggplot2')
	library('vegan')

	# Defining comparison pairs
	pairs <- list()
	pairs[[1]] <- c("'0'","'1'") 
	pairs[[2]] <- c("'0'","'2'")

	# Importing phyloseq object
	phy <- qza_to_phyloseq(Tab, Tree, Tax, Map, tmp='Temp')

	#corregge l'importazione della tassonomia silva
	phy <- importaTassonomiaSilva(phy)
	# Factorizing the dependent variable column
	sample_data(phy)[[variable]] <- as.factor(sample_data(phy)[[variable]])

	# BETA DIVERSITY

	for(level in c("Species","Genus","Family")){
	    for(pair in pairs){
	        beta_diversity(phy,level,variable,pair)
    }
}
