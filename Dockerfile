FROM ubuntu:18.04
LABEL description="Docker image containing all requirements for metagenomic pipeline"
COPY environment.yml /
RUN conda env create -f /environment.yml && conda clean -a
ENV PATH /opt/conda/envs/metagenomic_pip_beta/bin:$PATH
## Don't recache on each execution, do that once per build process
RUN qiime dev refresh-cache
