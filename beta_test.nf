params.mapping = "/home/adelaide/adelaide/Dottorato/Nextflow/met_pipeline/Metadata/mapping.txt"
params.table = "/home/adelaide/adelaide/Dottorato/Nextflow/met_pipeline/DADA2_output/dada2_gastrodata_table.qza"
params.taxa = "/home/adelaide/adelaide/Dottorato/Nextflow/met_pipeline/Taxonomic_assignment/taxonomy_classification_data_sklearn.qza"
params.tree = "/home/adelaide/adelaide/Dottorato/Nextflow/met_pipeline/Phylogenetic_tree/rooted_tree.qza"
params.outdir = "$baseDir"

file("$baseDir/Immagini").mkdir()

Channel
	.fromPath(params.mapping, type: 'file')
	.ifEmpty { error "Cannot find any metadata file at this path: ${params.mapping}"  }
	.into{ch_mapping_beta; ch_mapping_alpha}

Channel
	.fromPath(params.table, type: 'file')
	.ifEmpty { error "Cannot find any  file at this path: ${params.mapping}"  }
	.into{ch_table_qza_beta}

Channel
	.fromPath(params.taxa, type: 'file')
	.ifEmpty { error "Cannot find any file at this path: ${params.mapping}"  }
	.into{ch_classification_beta}

Channel
	.fromPath(params.tree, type: 'file')
	.ifEmpty { error "Cannot find any  file at this path: ${params.mapping}"  }
	.into{ch_rooted_beta}
	
ch_rooted_beta
	.combine(ch_table_qza_beta)
	.combine(ch_classification_beta)
	.combine(ch_mapping_beta)
	.set{ch_beta_diversity}

process beta_diversity { 

	input:
	
	set file(rooted_tree), file(dada2_data_table), file(taxonomy_classification_data), file(mapping) from ch_beta_diversity
	
	conda "/home/adelaide/anaconda3/envs/metaRJupy"
	
	"""
	beta_diversity.R ${dada2_data_table} ${mapping} ${rooted_tree} ${taxonomy_classification_data} "IBD"
	
	"""

}
